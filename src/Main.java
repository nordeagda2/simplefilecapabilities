import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by amen on 8/1/17.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String liniaZWejscia = "";

        while (!liniaZWejscia.equals("quit")) {
            System.out.print("Podaj komendę (delete/create_file/create_folder lub nazwa pliku którego informacje wypisać): ");
            liniaZWejscia = scanner.nextLine();
            String[] slowaWKomendzie = liniaZWejscia.split(" ");
            String pierwszeSlowo = slowaWKomendzie[0];

            if (pierwszeSlowo.toLowerCase().equals("del")) {
                File file = new File(pierwszeSlowo);
                file.delete();
                if (!file.exists()) {
                    System.out.println("File has been deleted");
                } else {
                    System.out.println("ERROR");
                }
            } else if (pierwszeSlowo.toLowerCase().equals("create directory")) {
                File file = new File(pierwszeSlowo);
                file.mkdir();
                if (file.exists()) {
                    System.out.println("Directory has been created");
                } else {
                    System.out.println("ERROR");
                }
            } else if (pierwszeSlowo.toLowerCase().equals("create file")) {
                File file = new File(pierwszeSlowo);
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (file.exists()) {
                    System.out.println("File has been created");
                } else {
                    System.out.println("ERROR");
                }
            } else {

                File file = new File(liniaZWejscia);
                if (file.isFile()) {
                    if (file.exists()) {
                        System.out.println("Plik istnieje: TAK");
                    } else if (!file.exists()) {
                        System.out.println("Plik istnieje: NIE");
                    }
                    System.out.println("ścieżka absolutna: " + file.getAbsolutePath());
                    System.out.println("ścieżka relatywna: " + file.getPath());
                    System.out.println("Wielkosć pliku: " + file.length());
                    System.out.println("Data ostatniej modyfikacji: " + file.lastModified());
                    System.out.println("Czy plik jest ukryty: " + file.isHidden());
                    System.out.println("Prawo dostępu do odczytu: " + file.canRead());
                    System.out.println("Prawo dostępu do zapisu: " + file.canWrite());
                    System.out.println("Prawo dostępu do wykonania: " + file.canExecute());
                } else if (file.isDirectory()) {
                    if (file.exists()) {
                        System.out.println("Katalog istnieje: TAK");
                    } else if (!file.exists()) {
                        System.out.println("Katalog istnieje: NIE");
                    }
                    System.out.println("Rozmiar katlogu: " + file.length());


                    for (File number : file.listFiles()) {
                        System.out.println(number + " " + number.length());
                    }
                }
            }
        }
    }


}
